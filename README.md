## JumpServer 安装


#### #环境
  - Linux x86_64
  - docker
  - docker-compose

#### #安装部署

```bash
#首次启动，启动部分服务
docker-compose -f docker-compose.init.yml up -d
#初始化数据
docker exec -it jms_core bash -c './jms upgrade_db'

#启动全部jumpserver服务
docker-compose up -d

#查看容器
docker-compose ps

#浏览器打开IP 默认用户密码 admin
```
